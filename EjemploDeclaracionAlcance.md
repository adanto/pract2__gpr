# Ejemplo Declaración alcance
> La declaración de alcance constituye un documento que describe los procesos necesarios 
> para garantizar que el proyecto incluye el trabajo requerido para completarlo con éxito

> Para ello incluye una serie de apartados que se indican a continuación:

## Misión del proyecto
> Define de forma amplia y general lo que se pretende conseguir gracias al proyecto

Ejemplo: La misión del proyecto es ofrecer a los estudiantes universitarios una aplicación para móvil que les facilite gestionar su cesta de la compra en supermercados

## Objetivos geneales y específicos
> El objetivo general equivale a la misión del proyecto, son conceptos prácticamente equiparables

> Los objetivos específicos concretan y precisan el objetivo general de acuerdo a la regla SMART

* Specific (Específico)
* Measurable (Mensurable, medible)
* Assignable (Asignable a alguien)
* Realistic (Realista)
* Time – based (Basado en el tiempo)

Ejemplos:

-Conseguir más de mil descargas en el plazo de un año tras el
lanzamiento de la aplicación

-Alcanzar un 80% de valoraciones positivas entre las
opiniones de los usuarios en las principales tiendas de
aplicaciones (Play Store, App Store, etc.) en el plazo de 6
meses

## Requisitos
> Son condiciones o especificaciones detalladas que debe
> cumplir el producto, servicio que esta previsto generar como resultado del proyecto

Ejemplos: 

-La aplicación debe poder sincronizarse con posibles aplicaciones de calendarios, previamente instaladas

-La aplicación debería permitir definir etiquetas y nombres personalizados para ciertos alimentos, así como agregar nuevos alimentos, no incluidos en la lista predefinida

## Restricciones
> Consisten en imitaciones en las opciones para el desarrollo del proyecto
> y proceden principalmente de fuentes externas al proyecto

Ejemplos: 
- La aplicación no debe pesar más de 5 MB, por las condiciones de la tienda online donde se comercializará
- El diseño de la aplicación se basará en ciertos colores corporativos, característicos de la organización donde trabajamos

## Asunciones y supuestos
> Se trata de situaciones o condiciones que se dan por supuesto al inicio del proyecto

Ejemplos: 
- Se supone que los estudiantes universitarios compran habitualmente los mismos productos en supermercados, de bajo precio y fáciles de preparar 
- Se supone que la mayoría de los estudiantes universitarios tienen móviles con acceso a internet

## Productos entregables
> Consisten en elementos tangibles, resultado del trabajo que estamos desarrollando en el proyecto

Ejemplos:
- Informes de diseño y análisis
- Código de la programación
- Informes sobre fallos detectados y corregidos en la fase de pruebas
- Aplicación finalizada

## Límites del proyecto
> Se trata de establecer condiciones o indicaciones, pero sobre todo qué no se va a hacer

Ejemplos:
- Esta aplicación no sugerirá recetas ni dará consejos sobre cómo preparar los alimentos (hervir pasta, freir un huevo…)
- Esta aplicación no dispondrá de alarmas para avisar de cuándo un producto está de oferta en el supermercado más cercano
- Esta aplicación no permitirá la comunicación entre usuarios para compartir recetas entre madre e hijo/a

## Alcance del producto o servicio
> Este apartado describe las características o especificaciones técnicas del resultado del proyecto (un producto o un servicio)

Ejemplos:
- La aplicación es compatible con los sistemas operativos A, B y C, permite el almacenamiento de hasta 50 listas distintas, formadas por un máximo de 200 productos 
y utiliza la conexión 3G o WiFi para detectar supermercados cercanos

## Criterios de éxito
> Establecen referencias para determinar si el desarrollo del proyecto ha permitido alcanzar los objetivos definidos al inicio

Ejemplo:
- Más del 75% de los usuarios que utilizan la aplicación habitualmente afirman que les facilita la gestión de la lista de la compra