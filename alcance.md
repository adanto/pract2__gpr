## Misión del proyecto
> Ofrecer una aplicación para gestionar emergencias.

## Objetivos geneales y específicos
> El objetivo general es facilitar a los usuarios la gestión de los
> servicios de emergencias.

* Tener la funcionalidad de asignar ambulancias y hospitales de la
manera más óptima en un plazo de 3 meses.
* Conseguir un 70% de votos positivos en la playStore en la versión móvil.
* Más del 60% de los hospitales de Valencia de soporte a la aplicación.

## Requisitos
- Se deberá permitir sincronizar con un sistema GPS incorporado en las ambulancias. 
- Se debe poder registrar nuevas emergencias sin perder los datos persistentes.
- Se debe poder consultar el estado de las emergencias y cambiarlo.
- Debe existir la posibilidad de obtener la información de la red de ambulancias y hospitales.
- Se ofrecerá la utilidad de autorrellenar los campos de emergencias con pacientes registrados anteriormente. Esto será posible teniendo el DNI del paciente.
- Los índices de las emergencias no podrán repetirse.
- Se podrán listar las emergencias.
- A la lista de emergencias se le puede aplicar ciertos filtros para ordenarlos en algún orden.
- Se debe poder sincronizar con una base de datos donde esten almacenadas las emergencias anteriores del paciente.

## Restricciones
- El diseño debe ser intuitivo ya que podría ser usado por personas
que no estén en sus plenas facultades.
- Tiempo de respuesta menor a 1.5 seg.

## Asunciones y supuestos
- Se supone que los pacientes ya tienen número de la seguridad social.
- Se supone que ya se dispone del historial médico.
- Se supone que los pacientes disponen de DNI.

## Productos entregables
- Informes de análisis y diseño
- Código de programación
- Informes sobre fallos detectados y corregidos en la fase de pruebas
- Versiones conforme se acaben los casos de uso
- Aplicación finalizada

## Límites del proyecto
- No permitirá seleccionar ambulancias o hospitales por el usuario
- No se ofrece un servicio de comunicación entre paciente-hospital
- No existe la posibilidad de modificar un perfil por el usuario

## Alcance del producto o servicio
- Se utiliza la misma aplicación tanto para Desktop como dispotivo
móvil gracias a Mobile First.
- Utilizará una base de datos SQL y un modelo singleton para acceder a
la misma.
- Puede establecer conexiones tanto 4G como Wi-Fi
- Compatible con todos los sistemas operativos (Excepto windows)

## Criterios de éxito
- Cada año aumenta el número de peticiones en un factor de 1.5
- El número de descargas aumenta exponencialmente